package mock;

import com.itechart.tdd.Account;

public interface AccountDao {
	
	public Account save(Account account);

}
