package com.itechart.tdd;

public class AccountBalanceException extends Exception {
	
	private Account account;
	
	public AccountBalanceException(Account account, String message) {
		super(message);
		this.account = account;
	}
	
	public AccountBalanceException(Account account, String message, Throwable cause) {
		super(message, cause);
		this.account = account;
	}

	@Override
	public void printStackTrace() {
		System.out.println(account);
		super.printStackTrace();
	}
	
	
	


}
