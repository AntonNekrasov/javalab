package com.itechart.maven.logging.service.neutral;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndependentService {
	
	private static final Logger logger = LoggerFactory.getLogger(IndependentService.class);
	
	public void run() {
		logger.debug("Multi-argument string: {}, {}, {}", "string argument", 15, new Contact("Alex", "901234"));
	}

}
