package com.itechart.maven.logging.service;

import com.itechart.maven.logging.service.bad.BadService;
import com.itechart.maven.logging.service.good.GoodService;
import com.itechart.maven.logging.service.neutral.IndependentService;

public class Demo {
	
	public static void main(String[] args) {
		AbstractService good = new GoodService();
		AbstractService bad = new BadService();
		IndependentService independent = new IndependentService();
		
		good.run();
		bad.run();
		independent.run();
	}

}
